import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'

@Injectable()
export class LineupService {

  constructor(private http: Http) { }

  getLineups() {
    return this.http.get('/assets/json/lineups.json').map(
      (res) => res.json()
    );
  }

}
