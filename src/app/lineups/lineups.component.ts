import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LineupService } from '../lineup.service';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-lineup',
  templateUrl: './lineups.component.html',
  styleUrls: ['./lineups.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({opacity: 1, transform: 'translateX(0)'})),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateY(-2%)'
        }),
        animate('0.3s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})
export class LineupsComponent implements OnInit {

  lineups = [];
  lineup = []
  lineupYear: String;
  d = new Date();
  currentYear = this.d.getFullYear();

  constructor(private route: ActivatedRoute, private lineupService: LineupService) {
    this.lineupYear = route.snapshot.params['lineup'] ? route.snapshot.params['lineup'] : this.currentYear;
    // this.getLineups(); 
    this.filterLineupsByYear('2018');
  }

  getLineups() {
    this.lineupService.getLineups().subscribe(
      (data) => {
          this.lineups = data;
          if (this.lineups.length > 0 ) {
            this.filterLineupsByYear(this.lineupYear);
          }
        }
    );
  }

  filterLineupsByYear(year) {
    if (!year) {
      year = this.currentYear.toString();
    }
    this.lineups.forEach(element => {
      if (element['year'] === year) {
        this.lineupYear = element['year'];
        this.lineup = element['bands'];
      }
    });
  };

  ngOnInit() {
    scroll(0, 0);
    this.getLineups(); 
    // if (this.lineups.length > 0 ) {
    //   this.filterLineupsByYear(this.lineupYear);
    // }
  }
}
