import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

  ngAfterViewInit() {
    var footerLinks = document.querySelectorAll('[data-footer-link]');

    for (var i=0; i<footerLinks.length; i++) {
        footerLinks[i].addEventListener('click', function(e) {
          document.getElementById('mmms-footer-toggle').click();
          e.preventDefault();
        });
    }
  }
}
