import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { LineupsComponent } from './lineups/lineups.component';
import { TicketsComponent } from './tickets/tickets.component';
import { FaqComponent } from './faq/faq.component';
import { CookoffComponent } from './cookoff/cookoff.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';

import { LineupService } from './lineup.service';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    LineupsComponent,
    TicketsComponent,
    FaqComponent,
    CookoffComponent,
    ContactComponent,
    HomeComponent,
    NavComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'lineups',
        component: LineupsComponent
      },
      {
        path: 'lineups/:lineup',
        component: LineupsComponent
      },
      {
        path: 'tickets',
        component: TicketsComponent
      }
      ,
      {
        path: 'faq',
        component: FaqComponent
      }
      ,
      {
        path: 'cookoff',
        component: CookoffComponent
      }
      ,
      {
        path: 'contact',
        component: ContactComponent
      }
    ])
  ],
  providers: [LineupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
